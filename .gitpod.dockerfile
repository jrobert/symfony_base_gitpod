FROM gitpod/workspace-full-vnc:latest

USER root

# install dependencies
RUN  wget https://get.symfony.com/cli/installer -O - | bash
RUN  mv /home/gitpod/.symfony/bin/symfony /usr/local/bin/symfony
RUN  chown -R gitpod:gitpod /home/gitpod/.symfony/
#ENV DEBIAN_FRONTEND=noninteractive
#RUN apt-get update \
#    && apt-get install -y xfce4 \
#    && apt-get clean && rm -rf /var/cache/apt/* && rm -rf /var/lib/apt/lists/* && rm -rf /tmp/*
#ENV WINDOW_MANAGER="startxfce4"
